Principles of Program Analysis -- Labolatory
============================================


-----

**Please do not publish your solutions.**

-----


* [WHILE language documentation](doc/while.md)
* [WHILE_MEM language documentation](doc/while_mem.md)


Build requirements
------------------

The tool requires ocaml 4.07 with additional packages:

* library `ocamlgraph` - tested with version 1.8.8
* library `cmdliner` - tested with version 10.02
* parser generator `menhir` - tested with version 20180905
* build system `dune` - tested with version 1.2.1

If you have `opam`  you can install desired ocaml version using command:

```
$ opam switch 4.07.0 
```

To use installed ocaml you need to type command given bellow. It configures
environment of current shell. It is good idea to add this command
to your `.bashrc` file.
```
$ eval `opam config env` 
```

All dependencies can be installed using command:

```
$ opam install ocamlgraph menhir cmdliner dune
```


Building
--------

Use the following command:

```
$ make
```

The `Makefile` script will use `dune` command to build program and then create
symlink in the `install` directory.


Testing
-------

Use the following command:

```
$ make test
```

The `Makefile` script will execute `./scripts/tester.py` testing script.

You can select algorithm for data flow analyses by setting ALGORITHM Makefile variable.


```
$ make test ALGORITHM=my_solver
```

On a fresh installation some tests may fail as some analyses are not
implemented (they are left as exercises for students).
